// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.impl;

import 'package:shelf/shelf.dart';
import 'package:path/path.dart' as p;
import 'package:matcher/matcher.dart';
import 'preconditions.dart';
import 'package:uri/uri.dart';
import 'package:shelf_path/shelf_path.dart';
export 'package:shelf_path/shelf_path.dart' show getPathParameters,
    getPathParameter;

import 'router.dart';


final _url = p.url;




class RouterImpl<R extends Router> extends _BaseRoute implements Router<R> {
  final List<_Route> _routes = <_Route>[];

  final Handler fallbackHandler;
  final HandlerAdapter handlerAdapter;
  final RouteableAdapter routeableAdapter;

  final PathAdapter pathAdapter;

  RouterImpl._internal(this.fallbackHandler, this.handlerAdapter,
                       this.routeableAdapter,
      PathAdapter pathAdapter, path, Middleware middleware)
      : this.pathAdapter = pathAdapter,
        super(pathAdapter(path), middleware);

  // TODO: we could expose 'path' at this level if it makes sense but
  // would require further refactoring first as the Router's _handler
  // will ignore the path
  RouterImpl({Function fallbackHandler,
    HandlerAdapter handlerAdapter: noopHandlerAdapter,
    RouteableAdapter routeableAdapter: noopRouteableAdapter,
    PathAdapter pathAdapter: uriTemplatePattern,
    Middleware middleware, path: '/' })
      : this._internal(handlerAdapter(fallbackHandler != null ?
          fallbackHandler : _send404),
          handlerAdapter, routeableAdapter,
          _wrapPathAdapter(pathAdapter), path, middleware);

  @override
  void add(dynamic path, List<String> methods, Function handler,
      { bool exactMatch: true, Middleware middleware,
        HandlerAdapter handlerAdapter }) {
    final ha = _maybeMergeHandlerAdapters(handlerAdapter);

    _routes.add(new _SimpleRoute(ha(handler), pathAdapter(path),
        methods, exactMatch, middleware));

  }

  @override
  void addAll(routeable,
              { dynamic path, Middleware middleware,
                HandlerAdapter handlerAdapter,
                RouteableAdapter routeableAdapter }) {
    final r = path != null ?
        child(path, middleware: middleware,
            handlerAdapter: handlerAdapter,
            routeableAdapter: routeableAdapter) : this;
    (r as RouterImpl).routeableAdapter(routeable)(r);
  }

  @override
  void get(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter }) {
    add(path, ['GET'], handler, middleware: middleware,
        handlerAdapter: handlerAdapter);
  }

  @override
  void post(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter }) {
    add(path, ['POST'], handler, middleware: middleware,
        handlerAdapter: handlerAdapter);
  }

  @override
  void put(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter }) {
    add(path, ['PUT'], handler, middleware: middleware,
        handlerAdapter: handlerAdapter);
  }

  @override
  void delete(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter }) {
    add(path, ['DELETE'], handler, middleware: middleware,
        handlerAdapter: handlerAdapter);
  }

  @override
  R child(dynamic path, { Middleware middleware,
      HandlerAdapter handlerAdapter,
      RouteableAdapter routeableAdapter,
      PathAdapter pathAdapter }) {
    final ha = _maybeMergeHandlerAdapters(handlerAdapter);

    final ra = routeableAdapter != null ? routeableAdapter : this.routeableAdapter;

    final pa = pathAdapter != null ? _wrapPathAdapter(pathAdapter) :
      this.pathAdapter;

    final child = createChild(ha, ra, pa, path, middleware);
    _routes.add(child);
    return child;
  }

  /// Subclasses should override to create an instance of the same type
  RouterImpl createChild(HandlerAdapter handlerAdapter,
                         RouteableAdapter routeableAdapter,
                         PathAdapter pathAdapter, path,
                         Middleware middleware) =>
      new RouterImpl._internal(fallbackHandler, handlerAdapter, routeableAdapter,
        pathAdapter, path, middleware);



  HandlerAdapter _maybeMergeHandlerAdapters(
      HandlerAdapter handlerAdapter) {
    var ha = null;
    if (handlerAdapter == null) {
      ha = this.handlerAdapter;
    }
    else if (handlerAdapter is MergeableHandlerAdpater &&
          this.handlerAdapter is MergeableHandlerAdpater) {
      MergeableHandlerAdpater haChild =
          handlerAdapter as MergeableHandlerAdpater;
      MergeableHandlerAdpater haParent =
          this.handlerAdapter as MergeableHandlerAdpater;

      ha = haChild.merge(haParent);
    }
    else {
      ha = handlerAdapter;
    }

    return ha;
  }

  @override
  void attach(dynamic path, R childRouter, { Middleware middleware,
      HandlerAdapter handlerAdapter, PathAdapter pathAdapter }) {
    final c = child(path, middleware: middleware, handlerAdapter: handlerAdapter,
        pathAdapter: pathAdapter);
    c._routes.add(childRouter);
  }

  // TODO: this won't work if top level has a path
  Handler get _basicHandler => _handleChildRoutes;

  Iterable<PathInfo> get fullPaths => _createFullPaths([], null);

  Iterable<PathInfo> _createFullPaths(Iterable<String> rootPathSegs,
                                      String method) {
    final ourPaths = super._createFullPaths(rootPathSegs, method)
        .map((pi) => pi.path);

    return _routes.map((r) =>
        r._createFullPaths(ourPaths, method))
          .expand((e) => e);
  }

//  @override
//  bool _canHandle(Request request) =>
//      super._canHandle(request) && _routes.any((r) => r._canHandle(request));


  dynamic _handleChildRoutes(Request request) {
    final route = _routes.firstWhere((r) =>
        r._canHandle(request.method, request.url),
        orElse: () => null);
    if (route != null) {
      return route._handle(request) ;
    }
    else {
      return fallbackHandler(request);
    }
  }


  @override
  List<String> get _methods => null;

  @override
  bool get _exactMatch => false;


  String toString() =>
      'Router at $_pathPattern; # routes ${_routes.length}';

}

// base class for the _Route Composite Pattern
abstract class _Route {
  Handler get handler;

  Handler get _basicHandler;
  List<String> get _methods;
  bool get _exactMatch;

  bool _canHandle(String method, Uri url);
  dynamic _handle(Request request);

  Iterable<PathInfo> _createFullPaths(Iterable<String> rootPathSegs,
                                      String method);

}

abstract class _BaseRoute implements _Route {
  final UriPattern _pathPattern;
  Handler get _basicHandler;
  Handler _handler;
  Handler get handler => _handler;

  _BaseRoute(this._pathPattern, Middleware middleware) {
    ensure(_pathPattern, isNotNull, '_pathPattern cannot be null');
    _handler =  middleware != null ?
        const Pipeline().addMiddleware(middleware).addHandler(_basicHandler) :
          _basicHandler;
  }


  @override
  bool _canHandle(String method, Uri url) {
    if (_methods != null && !_methods.contains(method)) {
      return false;
    }

    final patternMatches = _pathPattern.matches(url);

    if (!patternMatches) {
      return false;
    }

    if (!_exactMatch) {
      return true;
    }

    final uriMatch = _pathPattern.match(url);
    final remainingSegments = uriMatch.rest.pathSegments;

    return remainingSegments.length == 0
      || (remainingSegments.length == 1
        && (remainingSegments.first == '' || remainingSegments.first == '/')
    );
  }

  @override
  dynamic _handle(Request request) {
    // create a new request with the pathInfo and scriptName adjusted relative
    // to route path
    final newRequest = _createNewRequest(request);

    return handler(newRequest);
  }

  Request _createNewRequest(Request request) {
    var url = request.url;

    final uriMatch = _pathPattern.match(url);

    final requestSegs = url.pathSegments;

    final skipCount = request.scriptName.isNotEmpty && requestSegs.isNotEmpty
        && requestSegs.first == '/' ? 1 : 0;
    final remainingPath = _cleanRest(uriMatch.rest);

    final extraScriptPath = requestSegs.skip(skipCount).take(
        requestSegs.length - remainingPath.pathSegments.length);
    var newScriptName = _adjustPath(_url.joinAll(_url.split(request.scriptName)
        ..addAll(extraScriptPath)));

    var newPathInfo = _adjustPath(remainingPath.toString());

    if (_exactMatch && newPathInfo == url.toString()) {
      newPathInfo = '';
    }

    if (newPathInfo == '' && newScriptName == '') {
      newPathInfo = '/';
    }

    final newUrl = Uri.parse(newPathInfo);

    var newContext = request.context;
    if (uriMatch.parameters.isNotEmpty) {
      newContext = {}..addAll(request.context);
      addPathParametersToContext(newContext, uriMatch.parameters);
    }

    return request.change(context: newContext, url: newUrl,
        scriptName: newScriptName);

  }

  // workaround for https://github.com/google/uri.dart/issues/16
  Uri _cleanRest(Uri rest) {
    if (rest.hasAuthority || rest.fragment == '' || rest.query == '') {
      final builder = new UriBuilder.fromUri(rest);
      if (builder.fragment == '') {
        builder.fragment = null;
      }
      if (builder.host == '') {
        builder.host = null;
      }
      if (builder.queryParameters != null && builder.queryParameters.isEmpty) {
        builder.queryParameters = null;
      }

      return builder.build();
    }
    return rest;
  }

  String _adjustPath(String path) {
    if (path == '/') {
      return '';
    }
    if (path.isNotEmpty && !path.startsWith('/') && !path.startsWith('?')) {
      return _url.join('/', path);
    }

    return path;
  }

  Iterable<PathInfo> _createFullPaths(Iterable<String> rootPathSegs,
                                      String method) {
    final routeSegsStr = (_pathPattern is UriParser)
      ? (_pathPattern as UriParser).template.template
      : "......";

    // remove '/' unless first segment
    final relRouteSegsStr = rootPathSegs.isEmpty ? routeSegsStr :
      _relative(routeSegsStr);


    final newPathSegs = new List()..addAll(rootPathSegs)..add(relRouteSegsStr);

    final path = (_url.joinAll(newPathSegs) as String)
        .replaceAll('/{?', '{?'); // ugly

    final _newMethods = method != null ? [method] :
      _methods != null ? _methods : ['*'];
    return _newMethods.map((m) => new PathInfo(m, path));

  }

  String _relative(String routeSegsStr) {
    final sep = _url.separator;
    return routeSegsStr.startsWith(sep) ? routeSegsStr.substring(1)
        : routeSegsStr;
  }


}

class _SimpleRoute extends _BaseRoute {
  final Handler _basicHandler;
  final List<String> _methods;
  final bool _exactMatch;

  _SimpleRoute(this._basicHandler, UriPattern _pathPattern, this._methods,
      this._exactMatch, Middleware middleware)
      : super(_pathPattern, middleware) {
    ensure(_basicHandler, isNotNull, 'handler cannot be null');
  }


  String toString() =>
      '$_methods->$_pathPattern ${_exactMatch ? '(exact)' : ''}';

}

Response _send404(Request req) {
  return new Response.notFound("Not Found");
}


PathAdapter _wrapPathAdapter(PathAdapter pathAdapter) {
  return (path) {
    ensure(path, anyOf(isNull, new isInstanceOf<String>(),
        new isInstanceOf<UriPattern>()));

    if (path != null && path is UriPattern) {
      return path;
    }

    return pathAdapter(path);
  };

}
