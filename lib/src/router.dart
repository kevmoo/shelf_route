// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router;

import 'package:shelf/shelf.dart';
import 'package:path/path.dart' as p;
import 'package:uri/uri.dart';

import 'router_impl.dart' as impl;

/**
 * A [Handler] that routes to other handlers based on the incoming request
 * (path and method). Note there can be several layers of routers to facilitate
 * modularity.
 *
 * When the Router routes a request to a handler it adjusts the requests as follows:
 *
 * * the routes path is removed from the start of the requests pathInfo
 * * the routes path is added to the end of the requests scriptName
 *
 * e.g if original request had path info of `/banking` and scriptName was
 * `/abc` then when routing a request of `/banking/accounts` the new request
 * passed to the handler will have a pathInfo of `/accounts` and scriptName of
 * `/abc/banking`
 *
 * If [fallbackHandler] is provided it will be called if no routes match. If
 * omitted a default handler which returns a 404 response is used
 *
 * If [middleware] is included a pipeline will be created with the middleware
 * and [handler]
 *
 * A [HandlerAdapter] can be provided to allow handlers that do not conform
 * to the [Handler] typedef to be used. For example,
 * [shelf_bind](https://pub.dartlang.org/packages/shelf_bind) provides such
 * an adapter.
 *
 * A [RouteableAdapter] can be provided to allow functions that do not conform
 * to the [RouteableFunction] typedef to be used. For example,
 * [shelf_rest](https://pub.dartlang.org/packages/shelf_rest) provides such
 * an adapter.
 *
 */

Router router({ HandlerAdapter handlerAdapter: noopHandlerAdapter,
                RouteableAdapter routeableAdapter: noopRouteableAdapter,
                PathAdapter pathAdapter: uriTemplatePattern,
                Function fallbackHandler, Middleware middleware }) =>
    new impl.RouterImpl(handlerAdapter: handlerAdapter,
      routeableAdapter: routeableAdapter,
      pathAdapter: pathAdapter,
      fallbackHandler: fallbackHandler, middleware: middleware);

/// Used to specify that the Router should route requests to all HTTP methods
/// for a given route
const ALL_METHODS = null;


typedef void Printer(String s);

/// prints out a representation of all the routes within the router, using
/// the [printer] if provided or the standard Dart [print] function if not.
///
void printRoutes(Router router, { Printer printer: print }) {
  router.fullPaths.forEach((pi) {
    printer('${pi.method}\t->\t${pi.path}');
  });
}

class PathInfo {
  final String method;
  final String path;

  PathInfo(String method, String path)
      : this.method = method != null ? method : '*',
          this.path = path != null && !path.isEmpty ? path : '/';
}

/// A function capable of creating routes in a given [Router]
typedef RouteableFunction(Router router);


/// a function that adapts a function to be a shelf [Handler]
typedef Handler HandlerAdapter(Function handler);

/// a function that adapts a function to be a [RouteableFunction]
/// Note: the adapter must return a RouteableFunction. Unfortunately, the
/// analyzer complains if this is added to this typedef - circular types
typedef /* RouteableFunction */ RouteableAdapter(Function handler);


/// A [HanderAdapter] that can be merged with another handlerAdapter of the same
/// type
abstract class MergeableHandlerAdpater {
  HandlerAdapter merge(MergeableHandlerAdpater other);
}

typedef UriPattern PathAdapter(path);


abstract class Router<R extends Router> {

  // TODO: we could expose 'path' at this level if it makes sense but
  // would require further refactoring first as the Router's _handler
  // will ignore the path
  @deprecated // user router function
  factory Router({Function fallbackHandler,
    HandlerAdapter handlerAdapter: noopHandlerAdapter,
    RouteableAdapter routeableAdapter: noopRouteableAdapter,
    PathAdapter pathAdapter: uriTemplatePattern,
    Middleware middleware}) {
    return new impl.RouterImpl(handlerAdapter: handlerAdapter,
      routeableAdapter: routeableAdapter, pathAdapter: pathAdapter,
      fallbackHandler: fallbackHandler, middleware: middleware);
  }

  /**
   * Adds a route with the given [handler], [path] and [method].
   *
   * [path] may be either a String or a [UriPattern]. If path is a String it
   * will be parsed as a [UriParser] which means it is expected to conform to
   * a [UriTemplate].
   *
   * The [handler] must be of type [Handler] unless a custom [handlerAdapter]
   * has been set for this router. The [handlerAdapter] will be applied to
   * the provided [handler] to create the actual handler used.
   *
   * If [exactMatch] is true then a path such as `/foo/bar/fum` will not match
   * a path like `/foo/bar`, where as when it is false it will.
   *
   * If [middleware] is included a pipeline will be created with the middleware
   * and [handler]
   *
   * If [handlerAdapter] is provided it will be merged with the parent adapter
   * if it exists and both are of type [MergeableHandlerAdapter]. This allows
   * custom tweaking of creating the handler per route.
   */
  void add(dynamic path, List<String> methods, Function handler,
      { bool exactMatch: true, Middleware middleware,
        HandlerAdapter handlerAdapter });


  /// adds all the routes created by [routeable]'s [createRoutes] method
  /// to the current router if [path] is not provided or to a new child [Router]
  /// at [path].
  ///
  /// [routeable] must conform to the [RouteableFunction] typedef unless
  /// a [RouteableAdapter] is either passed to this method or already installed
  /// in this [Router].
  ///
  /// If [middleware] is included a pipeline will be created with the middleware
  /// and [handler]
  void addAll(routeable,
              { dynamic path, Middleware middleware,
                HandlerAdapter handlerAdapter,
                RouteableAdapter routeableAdapter });

  /// creates a route with method = GET
  void get(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter });

  /// creates a route with method = POST
  void post(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter });

  /// creates a route with method = PUT
  void put(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter });

  /// creates a route with method = DELETE
  void delete(dynamic path, Function handler, { Middleware middleware,
      HandlerAdapter handlerAdapter });

  /// adds a child [Router] at [path]
  R child(dynamic path, { Middleware middleware,
      HandlerAdapter handlerAdapter,
      RouteableAdapter routeableAdapter,
      PathAdapter pathAdapter });

  /// adds a child [Router] at [path]
  /// WARNING: Experimental and untested. Use at your own peril ;-)
  void attach(dynamic path, R childRouter, { Middleware middleware,
      HandlerAdapter handlerAdapter, PathAdapter pathAdapter });

  Iterable<PathInfo> get fullPaths;

  Handler get handler;

}

/// A class capable of creating routes in a given [Router].
/// In most cases `RouteableFunction` is simpler and more compact
abstract class Routeable<R extends Router> {
  void createRoutes(R router);
  void call(R router) => createRoutes(router);
}



Handler noopHandlerAdapter(Handler handler) => handler;

RouteableFunction noopRouteableAdapter(RouteableFunction routeable) => routeable;

UriPattern uriTemplatePattern(path) {

  // Note: we change '/' to '' as UriParser strips trailing '/'
  // and a '' path won't match agains a pattern of '/'
  final nonNullPath = path == null ? '' : path == '/' ? '' : path;
  return new UriParser(new UriTemplate(nonNullPath));
}
